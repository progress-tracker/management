# Progress Tracker

## User stories

### As a trainee, I want to take progress pictures

* Take progress pictures easily with a mobile device
* Upload them in a safe and available repository
* Make them accessible via a user-based authentication

### As a trainee, I want to track my weight and other numerical metrics

* Enter weight, BF% and other numerical metrics easily on a mobile device
* Enter weight, BF% and other numerical metrics easily via a web interface

### As a trainee, I want to visualize my progress over time

* Visualize progress pictures over time
* Visualize weigth and other numerical metrics over time

### As a trainee, I want to set specific numerical goals

* Set weight or other numerical metric goals
* Send a notification to the user when the goal is reached

### As a trainee, I want my progress tracker to integrate with a nutrition tracking app

* Integration with MyFitnessPal
* Water intake and macros notification
* Push daily weight into MyFitnessPal

### As a trainee, I want to see an overview of my current health

* Web-based dashboard
* Display all metrics, water intake, macros, etc.

